# Firejail profile for itch
# This file is overwritten after every install/update
# Persistent local customizations
include itch.local
# Persistent global definitions
include globals.local

# itch.io has native firejail/sandboxing support bundled in
# See https://itch.io/docs/itch/using/sandbox/linux.html

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include ${HOME}/.config/firejail/game-profile/disable-programs.inc

caps.drop all
netfilter
nodvd
nogroups
noinput
nonewprivs
noroot
notv
nou2f
novideo
protocol unix,inet,inet6,netlink
seccomp
shell none

private-dev
private-tmp

noexec /tmp

blacklist ${HOME}/Pictures
blacklist ${HOME}/vimwiki
blacklist ${HOME}/Videos
blacklist ${HOME}/Camera
blacklist ${HOME}/Archive
blacklist ${HOME}/Notebook
blacklist ${HOME}/Music
blacklist ${HOME}/second_hdd
blacklist ${HOME}/Git
blacklist ${HOME}/Backups
blacklist ${HOME}/Books
