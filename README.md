# dvr's dotfiles

Currently redoing everything for sway+wayland+fedora/nobara setup

## Dependencies
- zsh
- [autotiling](https://github.com/nwg-piotr/autotiling)
- mpd
- mpc
- ncmpcpp
- [pulsemixer](https://github.com/GeorgeFilipkin/pulsemixer)
- fcitx5-mozc (japanese input method)
- ydotool (xdotool replacement for wayland)
- wl-clipboard
- [swayNC](https://github.com/ErikReider/SwayNotificationCenter) (notifications)
- swaybg (static wallpaper)
- [mpvpaper](https://github.com/GhostNaN/mpvpaper) (animated wallpaper)
- [horriblename/lf](https://github.com/horriblename/lf) lf with sixel patch
- lf preview dependencies:
  - chafa (image)
  - lynx (html)
  - mediainfo (metadata)
  - ffmpegthumbnailer (video thumbnail)
  - pdftoppm (pdf thumbnail)
  - gnome-epub-thumbnailer (epub thumbnail)
  - atool (zip info)
  - odt2txt (odt)
  - gpg (decrypt gpg)
- [nsxiv](https://codeberg.org/nsxiv/nsxiv) nsxiv

# TODO
- complete dependencies list
- (maybe) an autoinstall script for fedora/nobara or rewrite DARBS
- remove unused scripts and dots
